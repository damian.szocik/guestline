type HotelPosition = {
  latitude: number;
  longitude: number;
  timezone: string;
};

type RoomOccupancy = {
  maxAdults: number;
  maxChildren: number;
  maxOverall?: number;
};

type RoomImage = {
  url: string;
  alt: string;
};

type RoomFacility = {
  code: string;
  name: string;
};

export type Room = {
  id: string;
  name: string;
  shortDescription?: string;
  longDescription: string;
  occupancy: RoomOccupancy;
  disabledAccess: boolean;
  bedConfiguration: string;
  images: RoomImage[];
  facilities: RoomFacility[];
};

export type Hotel = {
  id: string;
  name: string;
  description: string;
  address1: string;
  address2: string;
  postcode: string;
  town: string;
  country: string;
  countryCode: string;
  starRating: string;
  facilities: {
    code: string;
  }[];
  telephone: string;
  email: string;
  images: { url: string }[];
  checkInHours: string;
  checkInMinutes: string;
  checkOutHours: string;
  checkOutMinutes: string;
  position: HotelPosition;
  rooms: Room[];
};

export type ResponseError = {
  statusCode: number;
  error: string;
  message: string;
};
