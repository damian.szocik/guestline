import hotelsResponse from "../fixtures/responses/hotels_response_successful.json";
import roomsResponse from "../fixtures/responses/rooms_response_successful.js";

const hotelsResponseErrorMessage = "Rooms fetching failed message";
const roomsResponseErrorMessage = "Rooms fetching failed message";

beforeEach(() => {
  cy.task("clearNock");
});

it("displays response error message on failed hotels request", () => {
  const responseErrorMessage = "Hotels fetching failed message";
  cy.task("nock", {
    hostname: "https://obmng.dbm.guestline.net",
    method: "GET",
    path: "/api/hotels?collection-id=OBMNG",
    statusCode: 500,
    body: { message: responseErrorMessage },
  });
  cy.visit("/");
  cy.contains(responseErrorMessage).should("be.visible");
  cy.get("[data-testid=errorIcon]").should("be.visible");
});

it("displays response error message on failed rooms request", () => {
  cy.task("nock", {
    hostname: "https://obmng.dbm.guestline.net",
    method: "GET",
    path: "/api/hotels?collection-id=OBMNG",
    statusCode: 200,
    body: JSON.stringify(hotelsResponse),
  });
  cy.task("nock", {
    hostname: "https://obmng.dbm.guestline.net",
    method: "GET",
    path: "/api/roomRates/OBMNG/OBMNG1",
    statusCode: 500,
    body: { message: hotelsResponseErrorMessage },
  });
  cy.visit("/");
  cy.contains(hotelsResponseErrorMessage).should("be.visible");
  cy.get("[data-testid=errorIcon]").should("be.visible");
});

it("diplays each hotel's name and address", () => {
  cy.mockAllSuccessfulRequests(hotelsResponse, roomsResponse);
  cy.visit("/");
  cy.contains(roomsResponseErrorMessage).should("not.exist");
  cy.get("[data-testid=errorIcon]").should("not.exist");
  hotelsResponse.forEach((hotel) => {
    cy.contains(hotel.name).should("be.visible");
    cy.contains(hotel.address1).should("be.visible");
    if (hotel.address2) {
      cy.contains(hotel.address2).should("be.visible");
    }
  });
});

it("diplays each hotel's rating", () => {
  cy.mockAllSuccessfulRequests(hotelsResponse, roomsResponse);
  cy.visit("/");
  hotelsResponse.forEach((hotel) => {
    cy.get(
      `[data-testid="${hotel.id}-hotelCard"] [data-testid="star-selected"]`
    ).should("have.length", hotel.starRating);
  });
});

it("diplays each hotel's rooms' name, capacity and description", () => {
  cy.mockAllSuccessfulRequests(hotelsResponse, roomsResponse);
  cy.visit("/");
  hotelsResponse.forEach((hotel, hotelIndex) => {
    const { rooms } = roomsResponse[hotelIndex];
    cy.get(
      `[data-testid="${hotel.id}-hotelCard"] [data-testid="roomSection"]`
    ).as("hotelsRooms");
    cy.get("@hotelsRooms").should("have.length", rooms.length);
    cy.get("@hotelsRooms").each((hotelRoom, roomIndex) => {
      cy.wrap(hotelRoom.find('[data-testid="roomName"]')).should(
        "have.text",
        rooms[roomIndex].name
      );
      cy.wrap(hotelRoom.find('[data-testid="roomOccupancyAdults"]')).should(
        "have.text",
        `Adults: ${rooms[roomIndex].occupancy.maxAdults}`
      );
      cy.wrap(hotelRoom.find('[data-testid="roomOccupancyChildren"]')).should(
        "have.text",
        `Children: ${rooms[roomIndex].occupancy.maxChildren}`
      );
      cy.wrap(hotelRoom.find('[data-testid="roomDescription"]')).should(
        "have.text",
        rooms[roomIndex].longDescription
      );
    });
  });
});

it("filters hotels by rating", () => {
  cy.mockAllSuccessfulRequests(hotelsResponse, roomsResponse);
  cy.visit("/");
  cy.get('[data-testid="starFilter"] [data-testid="star-selected"]').should(
    "have.length",
    1
  );
  cy.get('[data-testid$="hotelCard"]').should(
    "have.length",
    hotelsResponse.length
  );
  cy.get('[data-testid="starFilter"] [data-testid="star"]').first().click();
  cy.get('[data-testid$="hotelCard"]').should("have.length", 4);
  cy.get('[data-testid$="hotelCard"]').each((hotelCard) => {
    cy.wrap(hotelCard.find('[data-testid="star-selected"]')).should(
      "have.length.at.least",
      2
    );
  });
  cy.get('[data-testid="starFilter"] [data-testid="star"]').first().click();
  cy.get('[data-testid$="hotelCard"]').should("have.length", 3);
  cy.get('[data-testid$="hotelCard"]').each((hotelCard) => {
    cy.wrap(hotelCard.find('[data-testid="star-selected"]')).should(
      "have.length.at.least",
      3
    );
  });
  cy.get('[data-testid="starFilter"] [data-testid="star"]').first().click();
  cy.get('[data-testid$="hotelCard"]').should("have.length", 2);
  cy.get('[data-testid$="hotelCard"]').each((hotelCard) => {
    cy.wrap(hotelCard.find('[data-testid="star-selected"]')).should(
      "have.length.at.least",
      4
    );
  });
  cy.get('[data-testid="starFilter"] [data-testid="star"]').first().click();
  cy.get('[data-testid$="hotelCard"]').should("have.length", 1);
  cy.get('[data-testid$="hotelCard"]').each((hotelCard) => {
    cy.wrap(hotelCard.find('[data-testid="star-selected"]')).should(
      "have.length.at.least",
      5
    );
  });
});

it("filters rooms by occupancy", () => {
  cy.mockAllSuccessfulRequests(hotelsResponse, roomsResponse);
  cy.visit("/");
  cy.get('[data-testid="adultsFilterValue"]').should("have.text", "0");
  cy.get('[data-testid="childrenFilterValue"]').should("have.text", "0");
  const allRooms = roomsResponse.reduce(
    (allRooms, hotelsRooms) => [...allRooms, ...hotelsRooms.rooms],
    []
  );
  cy.get('[data-testid="roomSection"]').should("have.length", allRooms.length);
  const getRoomsWithOccupancy = (adults = 0, children = 0) =>
    allRooms.filter(
      (room) =>
        room.occupancy.maxAdults >= adults &&
        room.occupancy.maxChildren >= children
    );
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(2).length
  );
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(3).length
  );
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(7).length
  );
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(2).length
  );
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(1).length
  );
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 1).length
  );
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 2).length
  );
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 3).length
  );
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 5).length
  );
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 3).length
  );
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 2).length
  );
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(0, 1).length
  );
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", allRooms.length);
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(1, 1).length
  );
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(2, 1).length
  );
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(2, 2).length
  );
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(3, 2).length
  );
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should(
    "have.length",
    getRoomsWithOccupancy(3, 3).length
  );
});

it("filters hotels by stars and rooms by occupancy combined", () => {
  cy.mockAllSuccessfulRequests(hotelsResponse, roomsResponse);
  cy.visit("/");
  cy.get('[data-testid="starFilter"] [data-testid="star"]').first().click();
  cy.get('[data-testid="starFilter"] [data-testid="star"]').first().click();
  cy.get('[data-testid$="hotelCard"]').should("have.length", 3);
  cy.get('[data-testid$="hotelCard"]').each((hotelCard) => {
    cy.wrap(hotelCard.find('[data-testid="star-selected"]')).should(
      "have.length.at.least",
      3
    );
  });
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 22);
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 9);
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 3);
  cy.get('[data-testid="adultsFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 1);
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 3);
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 1);
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="childrenFilterIncreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 0);
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="adultsFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="childrenFilterDecreaseButton"]').click();
  cy.get('[data-testid="roomSection"]').should("have.length", 23);
  cy.get('[data-testid="starFilter"] [data-testid="star-selected"]')
    .first()
    .click();
  cy.get('[data-testid="roomSection"]').should("have.length", 34);
});
