Cypress.Commands.add("mockAllSuccessfulRequests", (hotelsMock, roomsMock) => {
  cy.task("nock", {
    hostname: "https://obmng.dbm.guestline.net",
    method: "GET",
    path: "/api/hotels?collection-id=OBMNG",
    statusCode: 200,
    body: JSON.stringify(hotelsMock),
  });
  hotelsMock.forEach((hotel, index) => {
    cy.task("nock", {
      hostname: "https://obmng.dbm.guestline.net",
      method: "GET",
      path: `/api/roomRates/OBMNG/${hotel.id}`,
      statusCode: 200,
      body: JSON.stringify(roomsMock[index]),
    });
  });
});
