import axios from "axios";
import type { GetServerSideProps, NextPage } from "next";
import { useCallback } from "react";
import { ErrorMessage } from "../components/ErrorMessage/ErrorMessage";
import { HotelCard } from "../components/HotelCard/HotelCard";
import { HotelListHeader } from "../components/HotelListHeader/HotelListHeader";
import { NoResultsContainer } from "../components/shared/NoResultsContainer/NoResultsContainer";
import { useFiltersContext } from "../contexts/filters";
import { getHotels, getHotelsRooms } from "../services/api";
import { Hotel, ResponseError } from "../types";

type HomePageProps =
  | {
      errorMessage: string;
    }
  | { hotels: Hotel[] };

const Home: NextPage<HomePageProps> = (props) => {
  const [filtersState] = useFiltersContext();

  const filterHotelsByRating = useCallback(
    (hotel: Hotel) => Number(hotel.starRating) >= filtersState.stars,
    [filtersState.stars]
  );

  return (
    <>
      <HotelListHeader />
      <div className="flex justify-center pt-32 sm:pt-20 pb-10 px-5">
        <div className="max-w-screen-sm xl:max-w-screen-md 2xl:max-w-screen-xl w-full flex flex-col gap-8">
          {"errorMessage" in props ? (
            <ErrorMessage message={props.errorMessage} />
          ) : (
            <NoResultsContainer
              large
              fallbackMessage="No hotels matching selected criteria."
            >
              {props.hotels.filter(filterHotelsByRating).map((hotel) => (
                <HotelCard key={hotel.id} hotel={hotel} />
              ))}
            </NoResultsContainer>
          )}
        </div>
      </div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  try {
    const hotels = await getHotels();
    for (const [index, hotel] of hotels.entries()) {
      const rooms = await getHotelsRooms(hotel.id);
      hotels[index].rooms = rooms;
    }
    return {
      props: { hotels },
    };
  } catch (error) {
    let errorMessage = axios.isAxiosError(error)
      ? (error.response?.data as ResponseError).message
      : "Data fetching failed.";
    return {
      props: {
        errorMessage,
      },
    };
  }
};

export default Home;
