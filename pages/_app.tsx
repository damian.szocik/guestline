import type { AppProps } from "next/app";
import { FiltersContextProvider } from "../contexts/filters";
import "../styles/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <FiltersContextProvider>
      <Component {...pageProps} />
    </FiltersContextProvider>
  );
}

export default MyApp;
