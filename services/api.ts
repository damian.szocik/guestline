import axios from "axios";
import { Hotel, Room } from "../types";

export const getHotels = async () => {
  const { data } = await axios.get<Hotel[]>(
    "https://obmng.dbm.guestline.net/api/hotels?collection-id=OBMNG"
  );
  return data;
};

export const getHotelsRooms = async (hotelId: string) => {
  const {
    data: { rooms },
  } = await axios.get<{ rooms: Room[] }>(
    `https://obmng.dbm.guestline.net/api/roomRates/OBMNG/${hotelId}`
  );
  return rooms;
};
