import { Hotel } from "../../../types";
import { Star } from "../../shared/Star";
import { ThumbnailSlider } from "./ThumbnailSlider";

type CardHeaderProps = Pick<
  Hotel,
  "name" | "address1" | "address2" | "starRating" | "images"
>;

export const CardHeader = ({
  name,
  address1,
  address2,
  starRating,
  images,
}: CardHeaderProps) => (
  <header className="flex flex-wrap gap-4 border-b-2 p-4">
    <ThumbnailSlider images={images} />
    <div className="grow">
      <div className="font-medium text-lg">{name}</div>
      <div>{address1}</div>
      <div>{address2}</div>
    </div>
    <div className="flex" data-testid="starsRating">
      {Array.from(Array(5).keys()).map((_, index) => {
        return (
          <Star
            key={`${name}-star-${index}`}
            selected={index < Number(starRating)}
          />
        );
      })}
    </div>
  </header>
);
