import "keen-slider/keen-slider.min.css";
import { useKeenSlider } from "keen-slider/react";
import Image from "next/image";
import { useState } from "react";
import { Hotel } from "../../../types";

export const ThumbnailSlider = ({ images }: { images: Hotel["images"] }) => {
  const [, setCurrentSlide] = useState(0);
  const [loaded, setLoaded] = useState(false);
  const [sliderRef, instanceRef] = useKeenSlider<HTMLDivElement>({
    loop: true,
    initial: 0,
    slideChanged(slider) {
      setCurrentSlide(slider.track.details.rel);
    },
    created() {
      setLoaded(true);
    },
  });

  return (
    <div className="h-24 w-32 relative rounded-lg overflow-hidden">
      {images.length === 1 ? (
        <Image src={images[0].url} alt="hotel photography" layout="fill" />
      ) : (
        <>
          <div ref={sliderRef} className="keen-slider h-full">
            {images.map((image, index) => (
              <div key={image.url} className="keen-slider__slide">
                <Image
                  src={image.url}
                  alt={`hotel photography ${index + 1}`}
                  layout="fill"
                />
              </div>
            ))}
          </div>
          {loaded && instanceRef.current && (
            <>
              <Arrow
                left
                onClick={() => {
                  instanceRef.current?.prev();
                }}
              />

              <Arrow
                onClick={() => {
                  instanceRef.current?.next();
                }}
              />
            </>
          )}
        </>
      )}
    </div>
  );
};

const Arrow = ({ left, onClick }: { left?: boolean; onClick: () => void }) => (
  <svg
    onClick={(event) => {
      event.stopPropagation();
      onClick();
    }}
    className={`absolute top-1/2 -translate-y-2/4 cursor-pointer fill-white ${
      !left ? "right-1" : "left-1"
    }`}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    height="1rem"
    width="1rem"
  >
    {left && (
      <path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z" />
    )}
    {!left && <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z" />}
  </svg>
);
