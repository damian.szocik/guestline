import { Room } from "../../types";

export const RoomSection = ({ room }: { room: Room }) => (
  <div
    className="p-4 border-b-2 grid grid-cols-1 sm:grid-cols-[1fr,4fr] gap-6"
    data-testid="roomSection"
  >
    <div>
      <div className="font-medium mb-1" data-testid="roomName">
        {room.name}
      </div>
      <div data-testid="roomOccupancyAdults">
        Adults: {room.occupancy.maxAdults}
      </div>
      <div data-testid="roomOccupancyChildren">
        Children: {room.occupancy.maxChildren}
      </div>
    </div>
    <p data-testid="roomDescription">{room.longDescription}</p>
  </div>
);
