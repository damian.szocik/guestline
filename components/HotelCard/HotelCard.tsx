import { useCallback } from "react";
import { useFiltersContext } from "../../contexts/filters";
import { Hotel, Room } from "../../types";
import { NoResultsContainer } from "../shared/NoResultsContainer/NoResultsContainer";
import { CardHeader } from "./CardHeader/CardHeader";
import { RoomSection } from "./RoomSection";

type HotelCardProps = { hotel: Hotel };

export const HotelCard = ({ hotel }: HotelCardProps) => {
  const [filtersState] = useFiltersContext();
  const filterRoomsByOccupancy = useCallback(
    (room: Room) => {
      const { maxAdults, maxChildren } = room.occupancy;
      const { adults, children } = filtersState;
      return maxChildren >= children && maxAdults >= adults;
    },
    [filtersState.adults, filtersState.children]
  );

  return (
    <section
      className="border-2 border-gray-200 border-opacity-60 rounded-lg"
      data-testid={`${hotel.id}-hotelCard`}
    >
      <CardHeader
        name={hotel.name}
        address1={hotel.address1}
        address2={hotel.address2}
        starRating={hotel.starRating}
        images={hotel.images}
      />
      <NoResultsContainer fallbackMessage="No rooms matching selected criteria.">
        {hotel.rooms.filter(filterRoomsByOccupancy).map((room) => (
          <RoomSection key={room.id} room={room} />
        ))}
      </NoResultsContainer>
    </section>
  );
};
