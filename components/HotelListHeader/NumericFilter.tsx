type NumericFilterProps = {
  label: string;
  value: number;
  onChange: (newValue: number) => void;
};

export const NumericFilter = ({
  label,
  onChange,
  value,
}: NumericFilterProps) => (
  <div className="flex items-baseline text-xl gap-1">
    <div className="font-medium title-font text-lg text-gray-900 tracking-wider">
      {label}
    </div>
    <button
      data-testid={`${label.toLocaleLowerCase()}FilterIncreaseButton`}
      onClick={() => onChange(value + 1)}
    >
      +
    </button>
    <div data-testid={`${label.toLocaleLowerCase()}FilterValue`}>{value}</div>
    <button
      data-testid={`${label.toLocaleLowerCase()}FilterDecreaseButton`}
      onClick={() => value > 0 && onChange(value - 1)}
    >
      -
    </button>
  </div>
);
