import Image from "next/image";
import { useFiltersContext } from "../../contexts/filters";
import { NumericFilter } from "./NumericFilter";
import { StarFilter } from "./StarFilter";

export const HotelListHeader = () => {
  const [filtersState, setFiltersState] = useFiltersContext();
  return (
    <header className="w-full shadow-sm h-80 sticky -top-[17rem] z-10 bg-cover bg-center flex justify-center items-end border-b-2  border-gray-200 border-opacity-60">
      <Image
        src="/img/hero_bg.webp"
        alt="header section background"
        layout="fill"
        objectFit="cover"
      />
      <div className="border-2 bg-white p-4 border-gray-200 border-opacity-60 rounded-lg translate-y-2/4 flex flex-col sm:flex-row items-center gap-4">
        <StarFilter
          selectedStars={filtersState.stars}
          onSelect={(stars) => setFiltersState({ ...filtersState, stars })}
        />
        <NumericFilter
          label="Adults"
          value={filtersState.adults}
          onChange={(adults) => setFiltersState({ ...filtersState, adults })}
        />
        <NumericFilter
          label="Children"
          value={filtersState.children}
          onChange={(children) =>
            setFiltersState({ ...filtersState, children })
          }
        />
      </div>
    </header>
  );
};
