import { useState } from "react";
import { Star } from "../shared/Star";

type StarFilterProps = {
  selectedStars: number;
  onSelect?: (selection: number) => void;
};

export const StarFilter = ({
  selectedStars = 0,
  onSelect,
}: StarFilterProps) => {
  const [hoverIndex, setHoverIndex] = useState(0);
  return (
    <div className="flex" data-testid="starFilter">
      {Array.from(Array(5).keys()).map((_, index) => {
        const starNumber = index + 1;
        const selected = index < selectedStars;
        const hovered = index < hoverIndex;
        return (
          <Star
            key={index}
            onMouseEnter={() => setHoverIndex(starNumber)}
            onMouseLeave={() => setHoverIndex(0)}
            onClick={() => onSelect?.(starNumber)}
            selected={selected}
            hovered={hovered}
          />
        );
      })}
    </div>
  );
};
