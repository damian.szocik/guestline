import { ErrorIcon } from "./ErrorIcon";

export const ErrorMessage = ({ message }: { message: string }) => (
  <div className="flex p-4 justify-center items-center gap-4">
    <ErrorIcon />
    <div className="text-xl" data-testid="errorMessage">
      {message}
    </div>
  </div>
);
