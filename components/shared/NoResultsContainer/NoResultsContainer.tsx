import { ReactNode } from "react";
import { WarningIcon } from "./WarningIcon";

export const NoResultsContainer = ({
  children,
  fallbackMessage,
  large,
}: {
  children?: ReactNode[];
  fallbackMessage: string;
  large?: boolean;
}) =>
  children?.length ? (
    <>{children}</>
  ) : (
    <div
      className={`flex p-4 justify-center items-center gap-${
        large ? "4" : "2"
      }`}
    >
      <WarningIcon height={large ? "2.2rem" : "1.4rem"} />
      <div className={large ? `text-xl` : "text-base"}>{fallbackMessage}</div>
    </div>
  );
