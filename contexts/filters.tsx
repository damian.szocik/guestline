import {
  createContext,
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  useContext,
  useState,
} from "react";

export type FiltersState = {
  stars: number;
  adults: number;
  children: number;
};

type FiltersStateContextValue = [
  FiltersState,
  Dispatch<SetStateAction<FiltersState>>
];

const FiltersContext = createContext<FiltersStateContextValue>(
  {} as FiltersStateContextValue
);

export const FiltersContextProvider = ({ children }: PropsWithChildren) => {
  const filtersState = useState<FiltersState>({
    stars: 1,
    adults: 0,
    children: 0,
  });
  return (
    <FiltersContext.Provider value={filtersState}>
      {children}
    </FiltersContext.Provider>
  );
};

export const useFiltersContext = () => useContext(FiltersContext);
