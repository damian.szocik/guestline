This is a [Guestline](https://www.guestline.com/) recruitment challenge bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

At first install dependencies:

```bash
npm run dev
```

## Development

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Testing

There are Cypress e2e tests provided. To open Cypress GUI run:

```bash
npm run cypress:open
```

The following command runs the tests in headless mode:

```bash
npm run cypress:run
```

## Building project

```bash
npm run build
```

## Running project

To serve previously build project run:

```bash
npm run start
```
